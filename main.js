let firstNameElement = document.getElementById("firstName");
let firstNameErrorElement = document.getElementById("firstNameError");

let lastNameElement = document.getElementById("lastName");
let lastNameErrorElement = document.getElementById("lastNameError");

let emailElement = document.getElementById("email");
let emailErrorElement = document.getElementById("emailError");

let passwordElement = document.getElementById("password");
let passwordErrorElement = document.getElementById("passwordError");

let retypePasswordElement = document.getElementById("retypePassword");
let retypePasswordErrorElement = document.getElementById("retypePasswordError");

let checkboxElement = document.getElementById("checkbox");
let termsAndConditionsErrorElement = document.getElementById("termsAndConditionsError");

let signupContainerElement = document.getElementById("signupContainer");
let successContainerElement = document.getElementById("successContainer");

window.onload = () => {
    firstNameElement.value = "";
    lastNameElement.value = "";
    emailElement.value = "";
    passwordElement.value = "";
    retypePasswordElement.value = "";
    checkboxElement.checked = false;
}

let isFirstNameValid = false;
let isLastNameValid = false;
let isEmailValid = false;
let isPasswordValid = false;
let isRetypePasswordValid = false;
let isTermsAccepted = false;

let nameValidation = /^[A-Za-z]+$/;
let emailValidation = /^\w+([\..-]?\w+)*@\w+([\..-]?\w+)*(\.\w{2,})+$/;
let passwordValidation = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
// let uppercaseLetters = /^[A-Z]+$/;
// let lowercaseLetters = /[a-z]/;
// let specialCharacters = /[ !"#$%&'()*+,-./:;<=>?@[\\\]^_`{|}~]/;

let lowercaseCount = 0;
let uppercaseCount = 0;
let specialCharactersCount = 0;


let createAccountBtn = document.getElementById("createAccountBtn");

createAccountBtn.addEventListener("click", (event) => {
    if(firstNameElement.value === "") {
        firstNameErrorElement.textContent = "*Required First Name";
    } else if(!(firstNameElement.value !== "" && firstNameElement.value.match(nameValidation))) {
        firstNameErrorElement.textContent = "*Please Enter a Valid Name";
    }else {
        firstNameErrorElement.textContent = "";
        isFirstNameValid = true;
    }

    if(lastNameElement.value === "") {
        lastNameErrorElement.textContent = "*Required Last Name";
    } else if (!(lastNameElement.value !== "" && lastNameElement.value.match(nameValidation))) {
        lastNameErrorElement.textContent = "*Please Enter a Valid Name";
    } else {
        lastNameErrorElement.textContent = "";
        isLastNameValid = true;
    }

    if(emailElement.value === "") {
        emailErrorElement.textContent = "*Required Email Address";
    } else if (!(emailElement.value !== "" && emailElement.value.match(emailValidation))) {
        emailErrorElement.textContent = "*Please Enter a Valid Email Address";
    } else {
        emailErrorElement.textContent = "";
        isEmailValid = true;
    }

    if(passwordElement.value === "") {
        passwordErrorElement.textContent = "*Required Password";
    } else if(!(passwordElement.value !== "" && passwordElement.value.match(passwordValidation))) {
        passwordErrorElement.textContent = "*Please Enter a Valid Password";
    } else {
        passwordErrorElement.textContent = "";
        isPasswordValid = true;
    }

    if(retypePasswordElement.value === "") {
        retypePasswordErrorElement.textContent = "*Re-enter your password";
    } else if(!(passwordElement.value === retypePasswordElement.value)) {
        retypePasswordErrorElement.textContent = "Passwords do not match";
    } else {
        retypePasswordErrorElement.textContent = "";
        isRetypePasswordValid = true;
    }

    if(checkboxElement.checked === false) {
        termsAndConditionsErrorElement.textContent = "*Please accept terms and conditions";
    } else {
        termsAndConditionsErrorElement.textContent = "";
        isTermsAccepted = true;
    }

    if(isFirstNameValid && isLastNameValid && isEmailValid && isPasswordValid && isRetypePasswordValid && isTermsAccepted) {
        signupContainerElement.classList.add("displayContent");
        successContainerElement.classList.remove("displayContent");
    }
    
})

// passwordElement.addEventListener("keydown", (event) => {
//     if(uppercaseLetters.test(event.key) && (uppercaseCount < 1) && uppercaseLetters.test(passwordElement.value)) {
//         console.log("inside first condition");
//         passwordErrorElement.textContent = "Enter Atleast one Lowercase Letter and Password Should be minimum 6";
//         uppercaseCount += 1;
//     } else if(lowercaseLetters.test(event.key) && (lowercaseCount < 1) && lowercaseLetters.test(passwordElement.value)) {
//         passwordErrorElement.textContent = "Enter Atleast one Special Character and Password Should be minimum 6";
//         lowercaseCount += 1;
//     } else if(specialCharacters.test(event.key) && (specialCharactersCount < 1) && lowercaseLetters.test(passwordElement.value)) {
//         passwordErrorElement.textContent = "Enter minimum of 6 characters";
//         specialCharactersCount += 1;
//     } else {
//         passwordErrorElement.textContent = "";
//     }
// })